# TVPVOD

If you want to watch videos from https://vod.tvp.pl/ with adblock it's a program for You.  
Basic videos show annoying banner on top of video. This program opens video without it.

## EXAMPLE
```
python tvpvod.py https://vod.tvp.pl/video/the-voice-of-poland-8,przesluchania-w-ciemno-odc-9,33934432
```
