import sys
import webbrowser


VIDEO_URL_PATTERN = 'https://vod.tvp.pl/sess/tvplayer.php?object_id={}&autoplay=true'  # noqa


def get_video_id(url):
    parts = url.split(',')

    return parts[-1]


def open_player(video_id):
    url = VIDEO_URL_PATTERN.format(video_id)
    webbrowser.open(url)


def main():
    url = sys.argv[-1]
    video_id = get_video_id(url)
    open_player(video_id)


if __name__ == "__main__":
    main()
